# metastate - Returns Handling Department<!-- omit in toc -->
 
| &nbsp;           | &nbsp;                                                |
| ---------------- | ----------------------------------------------------- |
| Document Number: | **P2192R2**                                           |
| Date             | 2020-09-13                                            |
| Audience          | SG18 LEWG Incubator       |
| Author         | Dusan B. Jovanovic ( [dbj@dbj.org](mailto:dbj@dbj.org) ) |

*There are two ways of constructing a software design: One way is to make it so simple that there are obviously no deficiencies, and the other way is to make it so complicated that there are no obvious deficiencies. -- [C.A.R. Hoare](https://en.wikiquote.org/wiki/C._A._R._Hoare)*

## Table of Contents<!-- omit in toc -->

- [1. Abstract](#1-abstract)
- [2. Motivation](#2-motivation)
  - [2.1. No standard returns handling paradigm](#21-no-standard-returns-handling-paradigm)
  - [2.2. Run-Time requirements](#22-run-time-requirements)
  - [2.3. Interoperability](#23-interoperability)
  - [2.4. Energy requirement](#24-energy-requirement)
- [3. metastate](#3-metastate)
- [4. valstat](#4-valstat)
  - [4.1. my::valstat](#41-myvalstat)
- [5. Usage](#5-usage)
  - [5.1. Callers point of view](#51-callers-point-of-view)
  - [5.2. the API point of view](#52-the-api-point-of-view)
- [6. Summary](#6-summary)
- [7. References](#7-references)
- [8. Appendix A](#8-appendix-a)

## Revision history<!-- omit in toc -->

R2: More elaborate motivation.

R1: Marketing blurb taken out. Focused and short proposal. metastate in the front.

R0: "Everything is numbered" style. A lot of story telling and self marketing. Too long.

<div class="page"/>

## 1. Abstract

What this proposal is not? This is **not** yet another error handling solution, presumably based on some elaborate new type, this is a proposal about lightweight but effective handling of information returned from functions. 

This paper proposes applying an paradigm as a solution to a large proportion of deeply rooted, both C++ language and C++ community fragmentation problems[[3](#ref3)]. Also, this would be a library solution without a language change required. Materialized in standard C++ as one tiny template to be made part of the std lib. 

<span id="motivation" />

## 2. Motivation 

### 2.1. No standard returns handling paradigm

As of today, in the std lib, there are few (or more than few) non interoperable error handling paradigms, idioms and return types. From ancient to contemporary, still years old by now. None of them dealing with general rich returns. Added through time they have inevitably contributed to a rising technical debt present inside C++ std lib. 

Returns handling is the evolution of error handling. It is the true picture of real life API consuming algorithms, required to deal with wider scope as opposed to a simple "was there an error?". There is none of that in the std lib, but dotted on the C++ landscape are "home grown" solutions to the general returns handling in large C++ code bases. Think HTTP codes, as an example. And that is raising the level of complexity for all levels of application architectures developed with standard C++. 

For reasons of developing modern application architectures, requiring  general returns handling, developed in general for distributed systems and in particular in standard C++, author has formalised the "**metastate**" paradigm. 

Motivation is to aid in solving three requirements categories

1. Run-Time
2. Interoperability
3. Energy

### 2.2. Run-Time requirements

Key reason for appearance of C++ dialects, are to be found in strict run-time requirements. That means developing using the core language but without the std lib. Without std lib error handling idioms and types. Each such a solution is adding yet another nail in he coffin of interoperability. Motivation of this paper is also to try and offer an "over arching", but simple enough, returns handling paradigm applicable across standard C++ and dialects fragmenting away. 

**Strict run time requirements:**

Minimal list

1. do not use try / throw / catch[[6](#ref6)] 
   1. Why not? Performance and size are [primary requirements](https://thephd.github.io/freestanding-noexcept-allocators-vector-memory-hole)
   2. Side effects: 
      1. leading to "no constructors" paradigm. Proliferation of non standard solutions.
      2. Makes std lib unwanted
2. can not use \<system_error\>
   1. Why not? Various issues, please see [[14](#ref14)]
   2. Side effects:
      1. Home grown error codes subsystems
3. do no use `iostreams`
   1. Why not? Performance 
   2. Side effects: tight coupling with underlying OS

Honourable readership is aware of all the issues arising when trying to satisfy these requirements while using the std lib in the same time. Thus, author will be so bold not to delve into further detail in order to keep this paper simple and focused.

### 2.3. Interoperability

In danger of sounding like offering an panacea,  author will also draw the attention to the malleability of the metastate paradigm to be implemented with wide variety of languages used in developing components making an modern distributed system. 

Usability of an API is measured on all levels: from the code level, to the inter component level.

In order to design any API to be feasibly usable it has to be interoperable. That leads to three core requirements:

1. no "error code" as return value
   1. "special" error codes returned multiplied with different types multiplied with different context
   2. Learning curve for every function in every API
2. no "return arguments" 
   1. language specific, mutable argument solutions   
3. no special globals
   1. Think errno legacy 
   2. No pure functions

Some of the designed-in simplicity in this paper is an result of deliberate attempt to increase the interoperability (with other run-time environments and languages). Think standard C++ for: [WASM](https://developer.mozilla.org/en-US/docs/WebAssembly/C_to_wasm), [Node.JS](https://nodejs.org/api/addons.html), [Android](https://developer.android.com/studio/projects/add-native-code) and such. 

### 2.4. Energy requirement

Data centers energy consumption has become imperative. Most of the server side software is written in C/C++ . Pressure is on, to design and develop with energy consumption as an primary requirement, in mind. That means smaller and faster components.

## 3. metastate

Paradigm behind this proposal is called "*metastate*". Put in some API calling code, metastate idea is really rather simple and easy to comprehend. 

```cpp
// first learn about the special type retuned
// are you sure you use it properly?
auto value_or_error = legacy_function ();
```
Following the metastate paradigm we do not inspect values, we inspect the relationship of two instances returned.
```cpp
// there is no special type returned
// both instances can be in empty or not empty occupancy state
// fundamental types are ok
auto [value, status] = metastate_enabled_function ();
```
As meta-language is language of languages, **metastate** is "state of states". metastate is defined as an boolean AND combination of occupancy states of two instances. Namely: Value and Status. Value returned or not, together with status returned or not. A bit more formally.
```
; two possible occupancy states
has_value ::= true
empty ::= false
occupancy ::=  empty | has_value

; metastate is AND combination of two occupancies
; of two instances
metastate ::= occupancy(value) AND occupancy(status) 
``` 
There is no requirement for existence of metastate type. Combination of value *and* status occupancies is giving four possible metastates.

| Meta State Tag | Value occupancy  | op | Status occupancy  |
| ----- | ------- | ------- | --- |
| **Info**  | Has value | AND | Has value |
| **OK**    | Has value | AND | Empty     |
| **Error** | Empty     | AND | Has value |
| **Empty** | Empty     | AND | Empty     |

metastate names are just tags, they are not implying any kind of required behaviour from both callers or producers. Final metastate meaning is detached from these names. It is defined by adopters, their requirements and their coding standards.

Following is canonical capturing of metastates in standard C++. Here caller tests for all the four possible metastates returned.

```cpp
 // there is no 'special' return type 
 // and no special return values required
  auto [ value, status ] = metastate_enabled_function () ;

 // metastates capturing idiom 
 // capturing all four possible metastates
 // names are just in comments
 // there is no 'metastate' type
  if (   value &&   status )  { /* info */ }
  if (   value && ! status )  { /* ok   */ }
  if ( ! value &&   status )  { /* error*/ }
  if ( ! value && ! status )  { /* empty*/ }
```
metastate paradigm is particularly well suited to APIs which require their immediate caller to consider an non trivial scenario.

metastate serves in achieving clean algorithms for complex call consuming. The aded benefits are immediate applicability and ability in addressing all of the requirements from the [Motivation section](#motivation).
  
This proposal is not describing an [panacea](https://dictionary.cambridge.org/dictionary/english/panacea). It's value lies in deliberate of simplicity, delivering the adaptability. Universal adoption of the metastate paradigm, would be greatly aided by placing one tiny template in the std lib. This proposal requires no changes in the core language. Truth to be told there is no any type proposed. 

<!-- div class="page"/ -->

## 4. valstat

In order to achieve the required wide scope of the metastate there are are only few core mandates as described above. Metastate actual implementation is completely API and application context specific. Put simply the C++ std lib implementation must not dictate the usage beside supporting the paradigm. 

valstat is a name of the template, offering the greatest possible degree of freedom for metastate adopters. Implementation is simple, resilient, lightweight and feasible. Almost transparent.

The only requirement is to give callers and paradigm adopters, the opportunity to capture the four metastates, returned by API.

**Synopsis**

*std::valstat\<T,S\>* as a template is an generic interface whose aliases and definitions allow the easy metastates capturing by examining the state of occupancy of the 'value' and 'status' members.

```cpp
#pragma once
// std lib header: <valstat>
namespace std 
{
 template< typename T, typename S>
	struct [[nodiscard]] valstat
 {
    // both types must be able to 
    // simply and resiliently
    // exhibit state of occupancy
    // "empty" or "has value"
		using value_type = T ;
		using status_type = S ;

    // metastate is captured by combining
    // state of occupancy of these two 

		value_type   value;
		status_type  status;
 };
} // std
```
**Valstat definitions are vessels for carrying metastates**

*std::valstat* will be assuring the metastate presence in the standard C++. It will not mandate its usage. It should exist as a recommended, not mandatory implementation. It should be in a separate header *\<valstat\>*, to allow for complete decoupling from any of the std lib types and headers.

**Type requirements**

Both value and status type must offer an method that reveals their occupancy state. Presumably in a simple manner. Readily available example of that behaviour is [std::optional<T>](https://en.cppreference.com/w/cpp/utility/optional) type.

<a name="my_valstat" ></a>

### 4.1. my::valstat 
std::valstat variation we will actually use in examples in this proposal. As an illustration of the malleability of the proposed solution, we will solve the occupancy requirement imposed on valstat members by simply using std::optional. No thousands of lines of C++ is required for some special type. No need to be concerned about the implementation complexity[[13](#ref13)]. 
```cpp
// in some adopters namespace 
namespace my {
// ready to operate on almost any type
// std::optional rules permitting
template<typename T, typename S>
using valstat = std::valstat< 
         std::optional<T>, 
         std::optional<S> >;
} // my
```
In narrow C++ view of my::valstat it is not wrong to relax a metastate definition, as an "AND combination" of two std::optionals.

Now both producers and consumers have the generic readily applicable valstat template alias. Most of the time valstat consumers will use a structured binding. Let's see some ad-hoc examples of direct usage, no functions involved yet:
```cpp
   // ERROR metastate created
	auto [ value, status ] = my::valstat< int, int >{ {}, 42 };

// ERROR metastate captured
	if (! value && status ) {
		std::cout << "error status: " << *status ;
	}
```
It is quite ok and enough; to be using fundamental types for both value and status. No special types needed.
```cpp
   // OK metastate created
	auto [ value, status ] = my::valstat< int, int >{ 42, {} };

// OK metastate captured
if ( value && ! status ) { 
   cout << *value ;
}
```
Above we also see the valstat primordial coding idioms, starting to take shape.
<!-- div class="page"/ -->

## 5. Usage

It is admittedly hard to immediately see the connection between metastate implementations and the somewhat bold promises and promised wide spectrum of benefits, presented in the [motivation section](#motivation). 

There are many equally simple and convincing examples of metastate usage benefits. In order to keep this core proposal short we will first observe just one, but illustrative use-case. [Appendix A](#8-appendix-a) contains few more. 

### 5.1. Callers point of view

valstat instance carries (out of the function) information to be utilized by callers capturing the metastate. How and why (or why not) is the metastate capturing code shaped, that completely depends on the context of the app, the API logic and many other requirements dictated by adopters. 

Example bellow might be used by adopters operating on some database. In this illustration, adopters use the metastate to pass back (to the caller) full information, obtained during the database field fetching operation.

Again, there certainly could be, but here is no 'special' over-elaborated return type required. That is a good thing. Actually in the code bellow there is no return type needed by the callers, at all. metastate is implied, there is no 'metastate' type.

 ```cpp
 template<typename T>
   my::valstat<T, my::stat > 
   full_field_info
   (database::row row_, std::string_view field_name ) 
   noexcept ;
// full return handling after 
// the attempted integer value retrieval
// from the database field
auto [ value, status ] = full_field_info<int>( db_row, field_name ) ;
```
Primary objective is enabling callers comprehension of 
a full information passed out of the function. Of course satisfying the core requirements from the [motivation section](#2-motivation). In this scenario caller is capturing all four metastates.
```cpp
if (   value &&   status )  { 
   // metastate captured: info 
   std::cout << "\nSignificant value found: " << *value ;
   std::cout << "\nStatus is: " << my::status_message(*status) ;
  }

if (   value && ! status )  { 
   // metastate captured: ok 
   std::cout << "\nRetrieved value: " << *value ;
  }

if ( ! value &&   status )  { 
   // metastate captured: error 
   // in this example status contains an error message
   std::cout << "\nRead error: " <<my::status_message(*status) ;
  }

if ( ! value && ! status )  { 
   // metastate captured: empty 
   std::cout << "\nField is empty." ;
  }

 ```
Please do note, using the same paradigm it is almost trivial to imagine that same calling algorithm in e.g. JavaScript inside some node.js calling module. 

Also let us emphasize: Not all possible metastates need to be captured by the caller each and every time. It entirely depends on the API "contract", on the logic of the calling site, on application requirements and such.

### 5.2. the API point of view

Requirements permitting, API implementers are free to choose if they will use and return them all, one,two or three metastates. 

```cpp
// the API namespace
template<typename T>
my::valstat<T, my::stat > 
full_field_info
(database::row row_, std::string_view field_name ) 
// not throwing exceptions.
            noexcept 
{
   // sanity check
   if ( field_name.size() < 1) 
    // return ERROR metastate
      return { {}, my::stat::name_empty };      

   // using some hypothetical database API
   database::field_descriptor field = row_.fetch( field_name ) ;
 
    if ( field.in_error() ) 
    // return ERROR metastate
      return { {}, my::stat::db_api_error };      

    if ( field.is_empty() ) 
    // empty field is not an error
    // return EMPTY metastate 
      return { {}, {} };      

   T field_value{} ; 
   if ( false == field.data( field_value ) )
   // return ERROR metastate 
      return { {},  my::stat::type_cast_failed  }; 

 // API contract requires signalling if 'special' value is found
  if ( special_value( field_value ) )       
  // return INFO metastate
   return { field_value, my::stat::special_value }; 
// OK metastate halves 
   return { field_value, {} }; 
}
```
Perhaps obviously one does not even need to use std::valstat to employ the benefits of the metastate paradigm. It entirely depends on the adopters requirements. Using thread safe abstractions, or asynchronous processing is also not stopping the adopters to return the metastates from their API's.

Basically function signalling the metastate is simply returning two fields structure. With all the advantages and disadvantages imposed by the core language rules. Any kind of home grown but functional valstat will work too. As long as callers can capture the metastates by using it.

<!-- div class="page"/ -->

## 6. Summary

*Fundamentally, the burden of proof is on the proposers.* — B. Stroustrup, [[11]](#ref11)

Hopefully proving the evolution of error code handling into metastate capturing does not need much convincing. There are many real code situation where this paradigm can be used. metastate as common call returns handling paradigm requires to be ubiquitously adopted to become truly an instrumental to widespread interoperability. From micro to macro levels. From inside the code to inter components calls.

Developing standard C++ code using standard library, but in restricted run-time environments, is what one might call a "situation"[[3](#ref3)][[4](#ref4)][[11](#ref11)]. Author is certain honourable readership knows quite well why is that situation unresolved, and which actuall situation is that. There is no need for yet another [tractate](https://www.merriam-webster.com/dictionary/tractate), in the form of proposal. 

Authors primary target is to achieve widespread adoption of the metastate paradigm. metastate is not solving just the "error-signalling problem"[[11](#ref11)]. It is an paradigm, instrumental in solving the often hard and orthogonal set of run-time requirements described in the [motivation section](#2-motivation).

*"A paradigm is a standard, perspective, or set of ideas. A paradigm is a way of looking at something ... When you change paradigms, you're changing how you think about something..."* [vocabulary.com](https://www.vocabulary.com/dictionary/paradigm)

<!-- 
<h3>What might be the components of metastate possible success?</h3>

**metastate** is a concept (idea); valstat is metastate carrier with recommending behavioral pattern. The rest is optional and individual.

**metastate** is not mandatory
**metastate** has almost universal applicability.

**metastate** as a concept is simple, consistent, logical, easy to understand and feasible to use.

**valstat** and its user defined implementations, provide extremely light, easy to optimize code. It does not stand in the way of any C++ project to design and use project specific idioms based on valstat.
**valstat** as a template is an abstract interface. 

**metastate** has the potential of increasing interoperability for the C++ landscape at large. "Everyone" using the valstat idiom, means that everyone will consume each others API easier. Even if types returned are "foreign" to the logic of the caller. Compare this to catching exceptions from third party libraries, in one place. 
**metastate** is "legacy friendly". It can be very gradually introduced. No code breaks are necessary. 
-->

<!-- After reading [[P0976]](https://www.stroustrup.com/P0976-the-evils-of-paradigms.pdf) "The Evils of Paradigms", we might claim that metastate is **not** an "Elvis of Paradigms".  -->

metastate aims high. And the scope of metastate is rather wide. But this proposal is not C++ language extension, or an "panacea" , "silver bullet", "awesome paradigm" and some such "revolutionary thing". 

metastate is just an immediate and effective way of building bridges over one deeply fragmented part of the vast C++ territory. While imposing extremely little on adopters implementations and leaving the non-adopters to "proceed as before".
<!--
- metastate (and valstat) has the potential to improve and simplify the function return handling, universally across the C++ landscape at large. And, simplifications inevitably brings resilience.

- The more universally it is used, more valuable metastate idea will be. And if valstat has the prefix *std::* it has a chance of becoming truly adopted.

- metastate consuming code means local return handling. for that reason valstat consuming function are undeniably more complex. Writing safer code is not simpler v.s. writing the dangerous code.  Both writing functions producing and consuming valstat enabled API is undeniably more complex. But not necessarily slower code. Hardly anyone might be worried usage of *std::valstat* will increase compilation times or slow down execution times considerably.

- Naturally valstat usage leads to lighter executable because there is almost no need for exceptions machinery (besides throwing from constructors). Which also gives to the compiler the opportunity to fully optimize the code. 

- After all truly performance critical code need not use valstat. Fastest code is the one not checking function returns. And that is certainly not making it resilient, low maintenance code. A fine balance between fast and dangerous and safe but slower.

- Also metastate deliberate simplicity serves the purpose of **interoperability**. With C++ API's, with C API's or even with other languages depending on the run-time. Think [WASM](https://webassembly.org/), [NODE.JS](https://nodejs.org/api/addons.html), some [mobile OS](https://developer.android.com/studio/projects/add-native-code), etc.

2020Q3 C++ community urgently needs an standard and recommended solution for uniformly handling non trivial returns from functions. 

Barriers to the ubiquity of metastate, as ever with any new ideas, will be non-technical. But it is not standing in the way in any sense.
-->

Obstacles to metastate paradigm adoption are far from just technical. But here is at least an immediately usable attempt to chart the way out.

----------------------

<!-- <div class="page"/> -->

## 7. References

- <a id="ref0">[0]</a> B. Stroustrup (2018) **P0976: The Evils of Paradigms Or Beware of one-solution-fits-all thinking**, https://www.stroustrup.com/P0976-the-evils-of-paradigms.pdf

- <a id="ref1" >[1]</a> Ben Craig, Ben Saks, 
**Leaving no room for a lower-level language: A C++ Subset**,
http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p1105r1.html#p0709

- <a id="ref2" >[2]</a> Lawrence Crowl, Chris Mysen, **A Class for Status and Optional Value**, http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0262r1.html

- <a id="ref3" >[3]</a> Herb Sutter,**Zero-overhead deterministic exceptions**, https://wg21.link/P0709

- <a id="ref4" >[4]</a> Douglas, Niall, **SG14 status_code and standard error object for P0709 Zero-overhead deterministic exceptions**,  https://wg21.link/P1028

   - Douglas Niall, **Zero overhead deterministic failure – A unified mechanism for C and C++**, https://wg21.link/P1095

- <a id="ref5" >[5]</a>Vicente Botet, JF Bastien,  **std::expected** http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p0323r8.html

- <a id="ref6">[6] Craig Ben,   **Error size benchmarking: Redux** ,  http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1640r1.html</a>

- <a id="ref7">[7]</a> Vicente J. Botet Escribá, JF Bastien, **Utility class to represent expected object**,
http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/p0323r3.pdf

- <a id="ref8">[8]</a> Shoop Kirk, **Cancellation is not an Error**, http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1677r0.pdf

- <a id="ref9">[9]</a> Wikipedia **Empty String**, https://en.wikipedia.org/wiki/Empty_string

- <a id="ref10">[10]</a> "Your Dictionary" **Definition of empty**,  https://www.yourdictionary.com/empty

- <a id="ref11">[11]</a> Bjarne Stroustrup **P1947 C++ exceptions and alternatives**,  http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1947r0.pdf
- <a id="ref12">[12]</a> A Conversation with Anders Hejlsberg, Part II **The Trouble with Checked Exceptions**,  https://www.artima.com/intv/handcuffs.html
- <a id="ref13">[13]</a> Niall Douglass **Concerns about expected<T, E>
from the Boost.Outcome peer review**,  http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/p0762r0.pdf
- <a id="ref14">[14]</a> Library Evolution Working Group **Summary of SG14 discussion on <system_error>**,  http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p0824r1.html 

<div class="page"/>

## 8. Appendix A

Value of the programming paradigm is best understood by seeing the code using it. The more the merrier. Here are a few more simple examples illustrating the metastate applicability. All following the [initial set of requirements](#2-motivation).

**metastate as a value carrier**

An elegant solution to the "index out of bounds" problem. No exceptions, and no `exit()` in release builds. Using [my::valstat](#41-myvalstat) as already defined above. 

```cpp
// inside some sequence like container
// see the my::valstat above.
// my::errc is std::errc, minus <system_error> included. 
 my::valstat< T , my::errc >
     operator [] ( size_t idx_ ) noexcept
    {
        if ( ! ( idx_ < size_ ) )
        /* ERROR metastate */
        return { {}, my::errc::invalid_argument };
        /* OK metastate */
        return { data_[idx_] , {} };
    }
```
Metastate does carry the value. But only if metastates are OK or INFO. That pattern alone resolves few difficult and common API design issues.

**Uniform solutions for simpler usage**

Due to decades of legacy code development and debugging, inevitably its usage and complexity has become a problem. True even for C++ std lib. Somewhat trivial example: To find a char inside an std::string one can use std::find. To find a string inside the same std::string, one has to use std::string::find. metastate based solution out of this might be:

```cpp
// value is a position in a sequence being searched
template<class C, class T>
   constexpr  my::valstat< size_t, my::errc >
      my::find(C const & container_ , const T& value) noexcept;
```
using the overloads of above one can imagine a simple variety of implementations but all serving the same find consuming logic at the client side. Using the vector first.
```cpp    
    int n1 = 3 ;
    std::vector<int> v{0, 1, 2, 3, 4};
   auto [position,status] = my::find(v, n1);
```
Or using the string
```cpp
   char n1 = '3' ;
    std::string s{"01234"};
   // using std::string overloads 
   auto [position,status] = my::find(s, n1);
```
Find a string inside a string
```cpp
   string n2 = "23" ;
   auto [position,status] = my::find(s, n2);
```
metastate presence brings uniformity in algorithms using that API. That in turn makes applications simpler and more resilient. The same find logic is applicable to all containers and sequences alike. ( std::string::npos is not required ).
```cpp 
// always the same find consuming logic 
// based on metastate capturing 
if ( position )
   {
      // position is guaranteed to be inside boundaries
      my::process_the_find( v[*position] ) ;
   } else {
      // not found, status contains the reason
      my::process_the_status( *status ) ;
   }
```
Above is certainly doable without a metastate, but it will require yet another design and some new set of abstractions. Remember [std::to_chars](https://en.cppreference.com/w/cpp/utility/to_chars) use case. Otherwise one useful utility, but that returns yet another return type. An newcomer in std lib. Inevitably raising the technical debt of not having common returns handling paradigm, policy and implementation. 

**Decoupling from the legacy**

One can imagine using the metastate paradigm for developing simple but ubiquitous proxy API in front of the legacy API's.
```cpp
// notice here we even do not use my::valstat
inline std::valstat<FILE*, errno_t > 
  modern_fopen(const char* name_, const char* mode_) 
noexcept
{
    FILE* fp_{};
    int ec_ = fopen_s(&fp_, name_, mode_);

    if (NULL == fp_)
        // returning the ERROR metastate
        return { {} ,  ec_ }; 

    // returning the OK metastate
    return { fp_, {} }; // OK metastate
}
```
Very simple but fully metastate enabled. The usage:
```cpp
if (auto [ filep , errc ] = modern_fopen( "non_existent_file" , "w+" ); filep )
{
   // filep is a FILE *
   fprintf( filep, "OK" ) ;
}  else {
   // ad-hoc usage
   auto message = strerror( ( errc != 0 ? strerror (errc) : "no status") ) ;
}
```
Above decouples from decades of "special return values" ,`errno` globals and POSIX "hash defines". One can imagine the whole layer of metastate enabled functions, in front of the CRT legacy. 

---
