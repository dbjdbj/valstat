#  valstat for C

### Transparent return type

> [This](https://gitlab.com/dbjdbj/valstat) is a repository hosting a development of the standard (ISO) C++ proposal ID: P2192

In this folder are two options

- complicated on
- simple one

Some might disagree on this classification. I like the simple one. Easier to use, easier to understand the API's using it.

#### Contact

(c) 2019, 2020 by [dbj@dbj.org](mailto:dbj@dbj.org)

#### Content of this repository is licensed under the Creative Commons License [CC BY SA 4.0](LICENSE.md)


