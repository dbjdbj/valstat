

----------------------

<!-- <div class="page"/> -->

## References

- B. Stroustrup (2018) **P0976: The Evils of Paradigms Or Beware of one-solution-fits-all thinking **, <span id="ref0">[0]</span> https://www.stroustrup.com/P0976-the-evils-of-paradigms.pdf
- 
- <span id="ref1" href="ref1">[1]</span> Ben Craig, Ben Saks, 
**Leaving no room for a lower-level language: A C++ Subset**,
http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p1105r1.html#p0709

- <span id="ref2" href="ref2">[2]</span> Lawrence Crowl, Chris Mysen, **A Class for Status and Optional Value**, http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0262r1.html

- <span id="ref3" href="ref3">[3]</span> Herb Sutter,**Zero-overhead deterministic exceptions**, https://wg21.link/P0709

- <span id="ref4" href="ref4">[4]</span> Douglas, Niall, **SG14 status_code and standard error object for P0709 Zero-overhead deterministic exceptions**,  https://wg21.link/P1028

   - Douglas Niall, **Zero overhead deterministic failure – A unified mechanism for C and C++**, https://wg21.link/P1095

- <span id="ref5" href="ref5">[5]</span> Gustedt Jens, **Out-of-band bit for exceptional return and errno replacement**, http://www.open-std.org/jtc1/sc22/wg14/www/docs/n2361.pdf

   - Douglas Niall / Gustedt Jens, **Function failure annotation** , http://www.open-std.org/jtc1/sc22/wg14/www/docs/n2429.pdf


- Craig Ben,   **Error size benchmarking: Redux** , <span id="ref6">[6]</span> http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1640r1.html

- Vicente J. Botet Escribá, JF Bastien, **Utility class to represent expected object**,<span id="ref7">[7]</span>
http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/p0323r3.pdf

- Shoop Kirk, **Cancellation is not an Error**, <span id="ref8">[8]</span> http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1677r0.pdf

- Wikipedia **Empty String**, <span id="ref9">[9]</span> https://en.wikipedia.org/wiki/Empty_string

- "Your Dictionary" **Definition of empty**, <span id="ref10">[10]</span> https://www.yourdictionary.com/empty