
Please be informed these auxiliary folder and documents are still all on pre-alpha stage. Quite likely few of them have been rendered obsolete and will be moved into the [archive](../archive/).

### [Pre proposal text](architecture_design.md)

### [Implementation options](one_of_the_implementations.md)

### [valstat myth buster](valstat_myth_buster.md)

### [One of the possible C interops](one_of_the_possible_C_interops.md)

---
(c) 2019, 2020 by [dbj at dbj dot org](mailto:dbj@dbj.org)

#### Content of this repository is licensed under the Creative Commons License [CC BY SA 4.0](LICENSE.md)
